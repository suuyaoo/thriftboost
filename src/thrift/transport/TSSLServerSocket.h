/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#ifndef _THRIFT_TRANSPORT_TSSLSERVERSOCKET_H_
#define _THRIFT_TRANSPORT_TSSLSERVERSOCKET_H_ 1

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/shared_ptr.hpp>
#include <thrift/transport/TServerTransport.h>
#include <thrift/transport/TSSLSocket.h>

namespace apache {
namespace thrift {
namespace transport {

class TSSLSocketFactory;
class TSSLSocket;

/**
 * Server socket that accepts SSL connections.
 */
class TSSLServerSocket : public TServerTransport
{
public:
  const static int DEFAULT_BACKLOG = 1024;

  /**
   * Constructor.
   *
   * @param port    Listening port
   * @param factory SSL socket factory implementation
   */
  TSSLServerSocket(int port, const boost::shared_ptr<TSSLSocketFactory>& factory);

  /**
   * Constructor.
   *
   * @param port        Listening port
   * @param sendTimeout Socket send timeout
   * @param recvTimeout Socket receive timeout
   * @param factory     SSL socket factory implementation
   */
  TSSLServerSocket(int port,
                   int sendTimeout,
                   int recvTimeout,
                   const boost::shared_ptr<TSSLSocketFactory>& factory);

  ~TSSLServerSocket();

  void setSendTimeout(int sendTimeout);
  void setRecvTimeout(int recvTimeout);

  void setAcceptTimeout(int accTimeout);
  void setAcceptBacklog(int accBacklog);

  void setRetryLimit(int retryLimit);
  void setRetryDelay(int retryDelay);

  void setKeepAlive(bool keepAlive);

  void setTcpSendBuffer(int tcpSendBuffer);
  void setTcpRecvBuffer(int tcpRecvBuffer);

  void listen();
  void close();

  void interrupt();
  int getPort();

protected:
  boost::shared_ptr<TTransport> acceptImpl();
  boost::shared_ptr<TSSLSocket> createSocket();
  boost::shared_ptr<TSSLSocketFactory> factory_;
  int port_;

  boost::asio::ip::tcp::acceptor acceptor_;
  boost::asio::deadline_timer deadline_;

  int acceptBacklog_;
  int sendTimeout_;
  int recvTimeout_;
  int accTimeout_;
  int retryLimit_;
  int retryDelay_;
  int tcpSendBuffer_;
  int tcpRecvBuffer_;
  bool keepAlive_;

};

}
}
}

#endif
