/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#ifndef _THRIFT_TLOGGING_H_
#define _THRIFT_TLOGGING_H_ 1

#include <thrift/thrift-config.h>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/function.hpp>

namespace apache {
namespace thrift {

class TLogging
{
public:
  static void log_err(const char *fmt, ...);
  static void log_warn(const char *fmt, ...);
  static void log_info(const char *fmt, ...);
  static void log_debug(const char *fmt, ...);

  static void log(int severity, const char *msg);
  static void setLogFunc(boost::function<void (int, const char*)>);
private:
  static boost::function<void(int, const char*)> logfunc_;
};

enum 
{
  THRIFT_LOG_DEBUG = 1,
  THRIFT_LOG_INFO,
  THRIFT_LOG_ERR,
  THRIFT_LOG_WARN,
};

}
}

/**
 * T_GLOBAL_DEBUG_VIRTUAL = 0 or unset: normal operation,
 *                                      virtual call debug messages disabled
 * T_GLOBAL_DEBUG_VIRTUAL = 1:          log a debug messages whenever an
 *                                      avoidable virtual call is made
 * T_GLOBAL_DEBUG_VIRTUAL = 2:          record detailed info that can be
 *                                      printed by calling
 *                                      apache::thrift::profile_print_info()
 */
#if T_GLOBAL_DEBUG_VIRTUAL > 1
#define T_VIRTUAL_CALL() ::apache::thrift::profile_virtual_call(typeid(*this))
#define T_GENERIC_PROTOCOL(template_class, generic_prot, specific_prot)                            \
  do {                                                                                             \
    if (!(specific_prot)) {                                                                        \
      ::apache::thrift::profile_generic_protocol(typeid(*template_class), typeid(*generic_prot));  \
    }                                                                                              \
  } while (0)
#elif T_GLOBAL_DEBUG_VIRTUAL == 1
#define T_VIRTUAL_CALL() fprintf(stderr, "[%s,%d] virtual call\n", __FILE__, __LINE__)
#define T_GENERIC_PROTOCOL(template_class, generic_prot, specific_prot)                            \
  do {                                                                                             \
    if (!(specific_prot)) {                                                                        \
      fprintf(stderr, "[%s,%d] failed to cast to specific protocol type\n", __FILE__, __LINE__);   \
    }                                                                                              \
  } while (0)
#else
#define T_VIRTUAL_CALL()
#define T_GENERIC_PROTOCOL(template_class, generic_prot, specific_prot)
#endif

#endif // #ifndef _THRIFT_TLOGGING_H_
