/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#ifndef _THRIFT_CONCURRENCY_UTIL_H_
#define _THRIFT_CONCURRENCY_UTIL_H_ 1

#include <boost/chrono/chrono.hpp>
#include <boost/thread/thread.hpp>

namespace apache {
namespace thrift {
namespace concurrency {

/**
 * Utility methods
 *
 * This class contains basic utility methods for converting time formats,
 * and other common platform-dependent concurrency operations.
 * It should not be included in API headers for other concurrency library
 * headers, since it will, by definition, pull in all sorts of horrid
 * platform dependent stuff.  Rather it should be inluded directly in
 * concurrency library implementation source.
 *
 * @version $Id:$
 */
class Util
{
public:
  /**
   * Get current time as milliseconds from epoch
   */
  static int64_t currentTime() 
  {
    boost::chrono::system_clock::time_point now = boost::chrono::system_clock::now();
    boost::chrono::nanoseconds dur(now.time_since_epoch());
    return boost::chrono::duration_cast<boost::chrono::milliseconds>(dur).count();
  }

  /**
   * Get current time as micros from epoch
   */
  static int64_t currentTimeUsec()
  {
    boost::chrono::system_clock::time_point now = boost::chrono::system_clock::now();
    boost::chrono::nanoseconds dur(now.time_since_epoch());
    return boost::chrono::duration_cast<boost::chrono::microseconds>(dur).count();
  }

  /**
  * Get current time as nano from epoch
  */
  static int64_t currentTimeNsec()
  {
    boost::chrono::system_clock::time_point now = boost::chrono::system_clock::now();
    boost::chrono::nanoseconds dur(now.time_since_epoch());
    return dur.count();
  }

  static void thrift_usleep(int32_t ms)
  {
    boost::this_thread::sleep_for(boost::chrono::milliseconds(ms));
  }

  static void thrift_sleep(int32_t seconds)
  {
    boost::this_thread::sleep_for(boost::chrono::milliseconds(seconds));
  }


};

}
}
} // apache::thrift::concurrency

#endif // #ifndef _THRIFT_CONCURRENCY_UTIL_H_
