/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#ifndef _THRIFT_CONCURRENCY_IOSERVICE_H_
#define _THRIFT_CONCURRENCY_IOSERVICE_H_ 1

#include <thrift/concurrency/Thread.h>
#include <thrift/concurrency/ThreadManager.h>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/asio.hpp>

namespace apache {
namespace thrift {
namespace concurrency {

/**
 * io service for event loop
 *
 * @version $Id:$
 */
class IOService : public Runnable
{
public:

  IOService(int32_t hints = 1);

  ~IOService();

  boost::asio::io_service& getIOService()
  {
    return io_service_;
  }
  
  void run();

  void stop();

  static boost::asio::io_service& getDefaultIOService(int32_t hints = 1);

private:
  static void createInstance(int32_t hints);

private:
  boost::asio::io_service io_service_;

  static boost::shared_ptr<Thread> thr_;
  static boost::shared_ptr<IOService> instance_;
  static boost::once_flag flags_;
};

}
}
} // apache::thrift::concurrency

#endif // #ifndef _THRIFT_CONCURRENCY_IOSERVICE_H_
